<?php declare(strict_types=1);

namespace Infotechnohelp\TextModifier;

class StringModifier
{
    public static function ltrimDuplicates(string $string, string $characters): string
    {
        if (substr($string, 0, 1) !== $characters) {
            return $string;
        }

        return '_' . ltrim($string, $characters);
    }

    public static function lcfirst(string $string, string $ignoredPrefix = null)
    {
        if (strpos($string, $ignoredPrefix) !== 0) {
            return lcfirst($string);
        }

        $trimmedString = substr($string, strlen($ignoredPrefix));

        return $ignoredPrefix . lcfirst($trimmedString);
    }
}